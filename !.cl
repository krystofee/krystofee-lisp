(defun fact-rec (n acc)
    (cond
        (
            (= n 0)
            acc
        )
        (
            T
            (fact-rec (1- n) (* acc n))
        )
    )
)

(defun ! (n)
    (fact-rec n 1)
)
