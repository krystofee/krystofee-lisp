(defun in (lst elm)
    (cond
        (
            (equal (cdr lst) NIL)
            (equal (car lst) elm)
        )
        (
            (equal (car lst) elm)
            T
        )
        (
            T
            (in (cdr lst) elm)
        )
    )
)
