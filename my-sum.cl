(defun my-sum (a b)
    (cond
        (
            (< a 0)
            (my-sum (* -1 a) (* -1 b))
        )
        (
            (zerop a)
            b
        )
        (
            T
            (my-sum (1- a) (1+ b))
        )
    )
)
