(defun insert (root key)
    (cond
        (
            (equal root NIL)
            (make-node :key key)
        )
        (
            (< key (node-key root))
            (make-node
                :key (node-key root)
                :left (insert (node-left root) key)
                :right (node-right root)
            )
        )
        (
            T
            (make-node
                :key (node-key root)
                :left (node-left root)
                :right (insert (node-right root) key)
            )
        )
    )
)

(defun noop (&rest args) nil)

(defun print-inorder (root)
    (cond
        (
            (equal root NIL)
            NIL
        )
        (
            T
            (
                noop    (print-inorder (node-left root))
                        (print (node-key root))
                        (print-inorder (node-right root))
            )
        )
    )
)
