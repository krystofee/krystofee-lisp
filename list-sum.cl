(defun list-sum (x)
    (cond
        (
            (equal (car x) nil)
            0
        )
        (
            T
            (+ (car x) (list-sum (cdr x)))
        )
    )
)
