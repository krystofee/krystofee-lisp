(defun variadic-sum-rec (acc args)
    (cond
        (
            (equal (rest args) NIL)
            (+ acc (first (or args 0)))
        )
        (
            T
            (variadic-sum-rec (+ acc (first args)) (rest args))
        )
    )
)

(defun variadic-sum (a b &rest args)
    (variadic-sum-rec (+ a b) args)
)
