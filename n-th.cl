(defun n-th (lst n)
    (cond
        (
            (< n 0)
            (n-th (reverse lst) (1- (abs n)))
        )
        (
            (not (zerop n))
            (n-th (cdr lst) (1- n))
        )
        (
            T
            (car lst)
        )
    )
)
